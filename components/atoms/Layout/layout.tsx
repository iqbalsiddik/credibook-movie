import React, { Fragment, ReactNode } from "react";
import { ILayout } from "../../../interfaces/common";
import SEO from "../../organisms/SEO";
import Navbar from "./navbar";

const Layout: React.FC<ILayout> = ({ children, title, desc }) => {
  return (
    <div className="bg-main">
      <SEO title={title} description={desc} />
      <Navbar />
      <div className="container mx-auto my-10">{children}</div>
    </div>
  );
};

export default Layout;
