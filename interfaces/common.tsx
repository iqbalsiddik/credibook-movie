import { ReactNode } from "react";

export interface ForgotPassword {
  email: string;
}

export interface TFetchAxios {
  url: string;
  method: string;
  body?: any;
  token?: string;
}

export interface ILayout {
  children?: ReactNode;
  title: string;
  desc: string;
}

export interface IModal {
  show: boolean;
  close: () => void;
  url?: string;
}

export interface IMovie {
  id: number;
  title: string;
  vote_average: number;
  backdrop_path: string;
  overview: string;
}
