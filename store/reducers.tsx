import { combineReducers } from "redux";

import reducerHome from "./home/reducer";

const rootReducer = combineReducers({
  // public
  reducerHome,
});

export default rootReducer;
