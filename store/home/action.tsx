import environments, { api_key } from "../../environments/environments";
import fetchAxios from "../../helper/fetchAxios";
import { PDFDATA, POPULERMOVIE } from "./actionTypes";

export const actionUpComing = (data: any) => {
  return {
    type: POPULERMOVIE,
    payload: data,
  };
};

export const actionDataPDF = (data: any) => {
  return {
    type: PDFDATA,
    payload: data,
  };
};
