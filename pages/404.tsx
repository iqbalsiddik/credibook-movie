import React from "react";
import Link from "next/link";
import { FiArrowRight } from "react-icons/fi";

export default function Page404() {
  return (
    <div className="flex min-h-full items-center justify-center h-screen py-12 px-4 sm:px-6 lg:px-8 bg-main">
      <div>
        <div className="flex justify-center items-center mb-6">
          <img
            src={"https://credibook.com/images/logo-corporate-text.svg"}
            alt="logo binus"
            className="text-center"
          />
        </div>
        <div className="mb-2">
          <p className="text-[#0C51DC] text-center text-base text-white">404</p>
        </div>
        <div className="mb-2">
          <p className="text-5xl font-bold text-white">Page not found</p>
        </div>
        <div className="mb-6">
          <p className="text-[#9ca3af] text-white">
            Sorry, we couldn't find the page you're looking for
          </p>
        </div>
        <div className="flex justify-center items-center cursor-pointer">
          <Link href={"/home"}>
            <p className="text-[#0C51DC] text-white">Go back home</p>
          </Link>
          <span className="pl-2">
            <FiArrowRight style={{ color: "#fff" }} />
          </span>
        </div>
      </div>
    </div>
  );
}
