/* eslint-disable indent */
/* eslint-disable react/jsx-curly-spacing */
/* eslint-disable react/jsx-max-props-per-line */
/* eslint-disable quotes */
import "tailwindcss/tailwind.css";
import "react-toastify/dist/ReactToastify.css";
import type { AppProps } from "next/app";
import Head from "next/head";
import { Provider as ReduxProvider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import { ToastContainer } from "react-toastify";
import { persiststor, store } from "../store";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <ReduxProvider store={store}>
        <PersistGate loading={null} persistor={persiststor}>
          <Head>
            <link
              rel="icon"
              sizes="32x32"
              type="image/png"
              href="/public/credibook.webp"
            />
          </Head>
          <ToastContainer />
          <Component {...pageProps} />
        </PersistGate>
      </ReduxProvider>
    </>
  );
}
export default MyApp;
