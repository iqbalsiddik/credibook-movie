exports.id = 394;
exports.ids = [394];
exports.modules = {

/***/ 6062:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ layout)
});

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(9297);
// EXTERNAL MODULE: external "next-seo"
var external_next_seo_ = __webpack_require__(2364);
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(5282);
;// CONCATENATED MODULE: ./components/organisms/SEO.tsx




const SEO = props => {
  const {
    title = "Cardibook : Commerce platform for wholesale shopping in Indonesia",
    description = "Commerce platform for wholesale shopping in Indonesia"
  } = props;
  return /*#__PURE__*/jsx_runtime_.jsx(external_next_seo_.NextSeo, {
    title: title,
    description: description
  });
};

/* harmony default export */ const organisms_SEO = (SEO);
// EXTERNAL MODULE: ./node_modules/next/link.js
var next_link = __webpack_require__(1664);
// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(6731);
;// CONCATENATED MODULE: ./components/atoms/Layout/navbar.tsx
/* eslint-disable indent */

/* eslint-disable react/jsx-curly-spacing */

/* eslint-disable react/jsx-max-props-per-line */

/* eslint-disable quotes */






const NavLink = ({
  href,
  children
}) => {
  const router = (0,router_.useRouter)();
  const isActive = router.pathname === href;
  const className = isActive ? "text-white bg-blue-700" : "text-gray-700 hover:bg-gray-100 dark:text-gray-400 dark:hover:bg-gray-700 dark:hover:text-white";
  return /*#__PURE__*/jsx_runtime_.jsx(next_link.default, {
    href: href,
    children: /*#__PURE__*/jsx_runtime_.jsx("a", {
      className: `block py-2 pl-3 pr-4 rounded dark:bg-blue-600 ${className}`,
      children: children
    })
  });
};

function Navbar() {
  const {
    0: isShowNavbar,
    1: setIsShowNavbar
  } = (0,external_react_.useState)(false);
  return /*#__PURE__*/jsx_runtime_.jsx("nav", {
    className: "bg-gray-900 border-gray-200 px-2 sm:px-4 py-2.5 rounded dark:bg-gray-900",
    children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: "container flex flex-wrap items-center justify-between mx-auto",
      children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("a", {
        href: "/home",
        className: "flex items-center",
        children: [/*#__PURE__*/jsx_runtime_.jsx("img", {
          src: "https://credibook.com/images/logo-corporate-text.svg",
          className: "h-6 mr-3 sm:h-16",
          alt: "Flowbite Logo"
        }), /*#__PURE__*/jsx_runtime_.jsx("span", {
          className: "self-center text-xl font-semibold whitespace-nowrap text-white dark:text-white",
          children: "Movie"
        })]
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("button", {
        "data-collapse-toggle": "navbar-hamburger",
        type: "button",
        className: "inline-flex items-center p-2 ml-3 text-sm text-gray-500 rounded-lg hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600",
        "aria-controls": "navbar-hamburger",
        "aria-expanded": "false",
        onClick: () => setIsShowNavbar(!isShowNavbar),
        children: [/*#__PURE__*/jsx_runtime_.jsx("span", {
          className: "sr-only",
          children: "Open main menu"
        }), /*#__PURE__*/jsx_runtime_.jsx("svg", {
          className: "w-6 h-6",
          "aria-hidden": "true",
          fill: "currentColor",
          viewBox: "0 0 20 20",
          xmlns: "http://www.w3.org/2000/svg",
          children: /*#__PURE__*/jsx_runtime_.jsx("path", {
            "fill-rule": "evenodd",
            d: "M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z",
            "clip-rule": "evenodd"
          })
        })]
      }), /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: `${isShowNavbar ? "block" : "hidden"} w-full`,
        id: "navbar-hamburger",
        children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("ul", {
          className: "flex flex-col mt-4 rounded-lg bg-main dark:bg-gray-800 dark:border-gray-700",
          children: [/*#__PURE__*/jsx_runtime_.jsx("li", {
            children: /*#__PURE__*/jsx_runtime_.jsx(NavLink, {
              href: "/home",
              children: "Home"
            })
          }), /*#__PURE__*/jsx_runtime_.jsx("li", {
            children: /*#__PURE__*/jsx_runtime_.jsx(NavLink, {
              href: "/about",
              children: "About"
            })
          })]
        })
      })]
    })
  });
}
;// CONCATENATED MODULE: ./components/atoms/Layout/layout.tsx






const Layout = ({
  children,
  title,
  desc
}) => {
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
    className: "bg-main",
    children: [/*#__PURE__*/jsx_runtime_.jsx(organisms_SEO, {
      title: title,
      description: desc
    }), /*#__PURE__*/jsx_runtime_.jsx(Navbar, {}), /*#__PURE__*/jsx_runtime_.jsx("div", {
      className: "container mx-auto my-10",
      children: children
    })]
  });
};

/* harmony default export */ const layout = (Layout);

/***/ }),

/***/ 2431:
/***/ (() => {

/* (ignored) */

/***/ })

};
;