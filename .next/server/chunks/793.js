"use strict";
exports.id = 793;
exports.ids = [793];
exports.modules = {

/***/ 4123:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ CardMovie)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_icons_fi__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(6893);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1664);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__);





function CardMovie({
  data
}) {
  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx("div", {
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsxs)("div", {
      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsxs)("div", {
        className: "relative rounded-lg overflow-hidden",
        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx("img", {
          src: `https://image.tmdb.org/t/p/w500/${data === null || data === void 0 ? void 0 : data.poster_path}`,
          alt: "",
          className: "w-full h-full object-cover rounded-lg overflow-hidden"
        }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx("div", {
          className: "absolute top-8 right-0 bg-yellow-400 rounded-l-full w-10 text-right pr-1 text-yellow-600 font-bold",
          children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx("h1", {
            children: data === null || data === void 0 ? void 0 : data.vote_average
          })
        }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx("div", {
          className: "absolute inset-0 w-full h-full bg-main opacity-0 hover:opacity-100 transform transition bg-opacity-50 overflow-hidden flex flex-col items-center justify-center group",
          children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx(next_link__WEBPACK_IMPORTED_MODULE_1__.default, {
            href: `/detail/${data === null || data === void 0 ? void 0 : data.id}`,
            children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsxs)("div", {
              className: "rounded-full w-1/2 py-2 bg-indigo-500 font-semibold text-indigo-900 transform transition  opacity-0 group-hover:opacity-100 flex items-center justify-center space-x-1 cursor-pointer",
              children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx(react_icons_fi__WEBPACK_IMPORTED_MODULE_3__/* .FiInfo */ .H33, {
                className: "mt-1 text-white"
              }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx("span", {
                className: "text-white",
                children: "Detail"
              })]
            })
          })
        })]
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsxs)("div", {
        className: "flex items-center justify-between",
        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx("h1", {
          className: "text-white font-semibold text-xl mt-3",
          children: data === null || data === void 0 ? void 0 : data.title
        }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx("p", {
          className: "text-gray-400",
          children: data === null || data === void 0 ? void 0 : data.release_date.slice(0, 4)
        })]
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx("p", {
        className: "text-gray-500 line-clamp-3",
        children: data === null || data === void 0 ? void 0 : data.overview
      })]
    })
  });
}

/***/ }),

/***/ 8173:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ Modal)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_icons_fi__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6893);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__);





function Modal({
  show,
  close,
  url
}) {
  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.Fragment, {
    children: (() => {
      if (show) {
        return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("div", {
          className: `fixed z-10 inset-0 overflow-y-auto`,
          "aria-labelledby": "modal-title",
          role: "dialog",
          "aria-modal": "true",
          children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("div", {
            className: "flex items-center justify-center min-h-screen px-8 lg:px-4",
            children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("div", {
              className: "fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity",
              "aria-hidden": "true"
            }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("div", {
              className: "bg-main rounded-lg shadow-xl transform max-w-screen-lg w-full p-4 relative",
              children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("div", {
                onClick: () => {
                  close();
                },
                className: "absolute -top-2 -right-2 w-5 h-5 rounded-full bg-red-700 text-white flex items-center justify-center cursor-pointer",
                children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx(react_icons_fi__WEBPACK_IMPORTED_MODULE_2__/* .FiX */ .q5L, {
                  className: ""
                })
              }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("div", {
                className: "",
                children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("iframe", {
                  title: "titsd",
                  className: "w-full",
                  style: {
                    height: "60vh"
                  },
                  src: `https://www.youtube.com/embed/${url}?autoplay=1`,
                  frameBorder: "0" // allowFullScreen="allowfullscreen"

                })
              })]
            })]
          })
        });
      }
    })()
  });
}

/***/ }),

/***/ 1690:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (/* binding */ Skeleton)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__);



function Skeleton() {
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("div", {
    role: "status",
    className: "max-w-sm p-4 border border-gray-200 rounded shadow animate-pulse md:p-6 dark:border-gray-700",
    children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("div", {
      className: "flex items-center justify-center h-48 mb-4 bg-gray-300 rounded dark:bg-gray-700",
      children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("svg", {
        className: "w-12 h-12 text-gray-200 dark:text-gray-600",
        xmlns: "http://www.w3.org/2000/svg",
        "aria-hidden": "true",
        fill: "currentColor",
        viewBox: "0 0 640 512",
        children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("path", {
          d: "M480 80C480 35.82 515.8 0 560 0C604.2 0 640 35.82 640 80C640 124.2 604.2 160 560 160C515.8 160 480 124.2 480 80zM0 456.1C0 445.6 2.964 435.3 8.551 426.4L225.3 81.01C231.9 70.42 243.5 64 256 64C268.5 64 280.1 70.42 286.8 81.01L412.7 281.7L460.9 202.7C464.1 196.1 472.2 192 480 192C487.8 192 495 196.1 499.1 202.7L631.1 419.1C636.9 428.6 640 439.7 640 450.9C640 484.6 612.6 512 578.9 512H55.91C25.03 512 .0006 486.1 .0006 456.1L0 456.1z"
        })
      })
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("div", {
      className: "h-2.5 bg-gray-200 rounded-full dark:bg-gray-700 w-48 mb-4"
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("div", {
      className: "h-2 bg-gray-200 rounded-full dark:bg-gray-700 mb-2.5"
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("div", {
      className: "h-2 bg-gray-200 rounded-full dark:bg-gray-700 mb-2.5"
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("div", {
      className: "h-2 bg-gray-200 rounded-full dark:bg-gray-700"
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("div", {
      className: "flex items-center mt-4 space-x-3",
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("svg", {
        className: "text-gray-200 w-14 h-14 dark:text-gray-700",
        "aria-hidden": "true",
        fill: "currentColor",
        viewBox: "0 0 20 20",
        xmlns: "http://www.w3.org/2000/svg",
        children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("path", {
          "fill-rule": "evenodd",
          d: "M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-6-3a2 2 0 11-4 0 2 2 0 014 0zm-2 4a5 5 0 00-4.546 2.916A5.986 5.986 0 0010 16a5.986 5.986 0 004.546-2.084A5 5 0 0010 11z",
          "clip-rule": "evenodd"
        })
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("div", {
        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("div", {
          className: "h-2.5 bg-gray-200 rounded-full dark:bg-gray-700 w-32 mb-2"
        }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("div", {
          className: "w-48 h-2 bg-gray-200 rounded-full dark:bg-gray-700"
        })]
      })]
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("span", {
      className: "sr-only",
      children: "Loading..."
    })]
  });
}

/***/ }),

/***/ 2373:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "OB": () => (/* binding */ env),
/* harmony export */   "c2": () => (/* binding */ api_key),
/* harmony export */   "ZP": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* eslint-disable import/no-anonymous-default-export */
const env = "https://api.themoviedb.org/3/movie";
const api_key = "bcdd984cbc21fd82bb06b743ef386b5c";
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ({
  api: {
    popular: `${env}/popular`
  }
});

/***/ }),

/***/ 3580:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2376);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_toastify__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2034);
/* harmony import */ var react_toastify__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_toastify__WEBPACK_IMPORTED_MODULE_1__);



const fetchAxios = async props => {
  const {
    url,
    method,
    body,
    token
  } = props;

  if (token) {
    (axios__WEBPACK_IMPORTED_MODULE_0___default().defaults.headers.common.Authorization) = `Bearer ${token}`;
  }

  try {
    const response = await axios__WEBPACK_IMPORTED_MODULE_0___default()({
      method: method,
      url: url,
      data: body
    });

    if (response.status <= 299) {
      return Promise.resolve(response);
    } else if (response.status === 400) {
      react_toastify__WEBPACK_IMPORTED_MODULE_1__.toast.error(response.message);
      window.location.href = "/404";
      return Promise.resolve(response);
    } else if (response.status === 500) {
      react_toastify__WEBPACK_IMPORTED_MODULE_1__.toast.error(response.message);
    } else if (response.status === 404) {
      react_toastify__WEBPACK_IMPORTED_MODULE_1__.toast.error(response.message);
    } else if (response.status === 401) {
      react_toastify__WEBPACK_IMPORTED_MODULE_1__.toast.error(response.message);
    } else {
      react_toastify__WEBPACK_IMPORTED_MODULE_1__.toast.error(response.message);
    }
  } catch (err) {
    const error = err.toJSON();

    if (error.status === 422) {
      var _err$response, _err$response$data;

      react_toastify__WEBPACK_IMPORTED_MODULE_1__.toast.error((_err$response = err.response) === null || _err$response === void 0 ? void 0 : (_err$response$data = _err$response.data) === null || _err$response$data === void 0 ? void 0 : _err$response$data.message);
      return Promise.resolve(error);
    } else if (error.status === 401) {
      react_toastify__WEBPACK_IMPORTED_MODULE_1__.toast.error(error.message);
      return Promise.resolve(error);
    } else if (error.status === 500) {
      react_toastify__WEBPACK_IMPORTED_MODULE_1__.toast.error(error.message);
    }

    return Promise.resolve(error);
  }
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (fetchAxios);

/***/ })

};
;