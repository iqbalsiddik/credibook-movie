module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        main: "#0D1024",
        second: "#171833",
        input: "#16192D",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
